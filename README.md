# LuNES
[![](https://dcbadge.vercel.app/api/server/xkWV3b6ztU)](https://discord.gg/xkWV3b6ztU)

NES emulator

[Documentation](https://henny022.gitlab.io/lunes/)

## Credits
These sources have been a huge help in implementing LuNES:
- https://sotrh.github.io/learn-wgpu/
