# Introduction
This book documents the LuNES NES emulator, as well as my journey in implementing it.

If you wanna talk about the emulator, join my [discord server](https://discord.gg/xkWV3b6ztU), it has a channel dedicated to it.

<div class="warning">
Blocks like this one mark TODOs that are yet to be implemented in LuNES
</div>
