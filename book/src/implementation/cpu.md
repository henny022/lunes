# Implementing the CPU
The NES has a 6502 based CPU.
The major difference to a normal 6502 is the missing decimal mode.

What is most important to when implementing CPU internals is to find a good balance between keeping the code easy to read and write, as well as leveraging the rust type system to make invalid state not representable.
The available opcodes are grouped by addressing mode and opcode class.
The available addressing modes are:
- Implied / (Accumulator)
- Immediate
- Zero Page
- Absolute
- Zero Page Indexed
- Absolute Indexed
- Indexed Indirect
- Indirect Indexed (yes, this is something else than the above, yes, that is very confusing at times)
- Absolute Indirect (only available to JMP)
- Relative (Branches)

The opcode classes are:
- Read instructions
- ReadModifyWrite instructions
- Write instructions
- Branch instructions
- JMP

Nops are implemented as read instructions that do nothing.

## Clock
The CPU is clocked by the phi0 clock, derived from the master clock by dividing it by 12 (16 for PAL).
Internally the 6502 derives two new clock signals from this, phi1 and phi2 [^timing].
These divide each cycle into two parts.

LuNES emulates each CPU cycle in two phases in `CPU::phi1` and `CPU::phi2`.
The content of these two functions roughly match what happens during the respective clock phases, at least as far as outward observable behavior is concerned.
During `phi1` the CPU outputs to the address bus and on write cycles writes to the data bus.
During `phi2` it reads the the value from the data bus on read cycles.

## Memory Bus Access
Every cycle of the phi0 clock is either a read or write cycle.
How the CPU accesses the bus for each of the different addressing modes is implemented as documented in `6502_cpu.txt`[^6502txt].

## Instruction Prefetch
The 6502 fetches the next opcode during the last cycle of the previous instruction.
On some instructions this prefetch cycle also executes part of the previous instruction.
This "pipelining" is not visible from the outside, only making some instructions look "shorter" that they actually are.

LuNES simplifies this by mushing the internal effects into the previous cycle.
This technically causes an incorrect internal state, but this difference is not observable.

<div class="warning">
maybe change `FetchInstruction` to have a `ReadOp` parameter?
see if this ever simplifies anything?
</div>

## Illegal instructions
The 6502 has a few illegal or "unofficial" instructions.
They are less unofficial and more the instruction decoder glitching out when trying to decode a non existing instruction.

### Register AX
A few of the glitched instructions target both the A and X register.
This happens because the register load/store instructions use two bit to encode the register.
`00` refers to Y, `01` to A and `10` to X.
If `11` is used instead, the instruction decodes same as with `01` referring to A but uses register X as well.
On load that means, the value is loaded into both registers.
On store, the content from both registers is anded because of open collector bus stuff[^6502txt].

LuNES implements behavior this with a AX pseudo register available to these instructions.

### Read Modify Write Instructions
The majority of the illegal instructions fall into a category that combines one of the read-modify-write instructions with one of the read instructions.
This manifests in the RMW instruction being executed as normal, followed by running the read operation with the modified value.
In most addressing modes this runs without conflicts, because the read part is executed one cycle later than the RMW part, during the hidden instruction prefetch cycle.
With immediate addressing this breaks down with some wild instruction, some of with have analog effects inside the CPU affecting the results[^65xx].

## Interrupts and Reset
The interrupt/reset sequence of the 6502 is as follows:
- push the high byte of the program counter
- push the low byte of the program counter
- push the P register
- read the low byte of the handler address
- read the high byte of the handler address

The handler addresses are at:
- 0xFFFA-0xFFFB: NMI
- 0xFFFC-0xFFFD: Reset
- 0xFFFE-0xFFFF: IRQ/BRK

Before starting to execute an instruction, in an interrupt is pending, current instruction is forced to `BRK`(`0x00`).

The during the reset, the same stack pushes happen as during an interrupt, but the read/write line is set to read, so the corresponding stack location is read instead of written[^pagetable_reset].

LuNES initializes all CPU registers to 0 (including the stack pointer and program counter) and all flags cleared, except for the interrupt flag, which is set.
The CPU starts execution with the interrupt routine for the reset vector.
After executing the reset sequence, the CPU is in the state described at [^reset].

<div class="warning">
[^pagetable_reset] lists PC as initialized to 0x00ff.
Don't know how accurate that is because I can't get Visual6502 to give me a reset sequence like that.
Shouldn't matter since this only affects the two read cycles at the very beginning of the reset.
Since both `0x0000` and `0x00ff` are in zero page RAM, these reads should have no side effects and hence this difference should not matter for the sake of this emulator.
Fix this once I put some probes into my NES.
</div>

## Source
My different sources and roughly what information I pulled from them.

[^timing]: [github.com/maarten-pennings/6502](https://github.com/maarten-pennings/6502/tree/master/4ram) timing diagrams (not specific to NES, but behavior should match up well enough)

[^6502txt]: [6502_cpu.txt](https://www.nesdev.org/6502_cpu.txt) bus behavior of the different addressing modes (as maybe apparent from my choices on CPU state representation).

[^65xx]: [65xx Processor Data](https://www.romhacking.net/documents/318/) behavior of illegal instructions

[^pagetable_reset]: [pagetable.com: Internals of BRK/IRQ/NMI/RESET on a MOS 6502](https://www.pagetable.com/?p=410)

[^reset]: [nesdev: CPU power up state](https://www.nesdev.org/wiki/CPU_power_up_state)
