# PPU
The NES picture processing unit is responsible for drawing an image to the screen.

# Tiles
The NES displays images based on 8x8 tiles.
The tile data is stored in the so-called pattern table.
The pattern table is on the PPU memory bus at location `0x0000`-`0x1fff`.
It is logically split into two equally sized halves.

Each tile is encoded in 16 byte.
Tiles have a bit depth of 2, allowing for 4 colors.
Color 0 is always transparent, leaving three colors in the palette.
The tile is stored in two planes.
Each plane is made up of 8 byte, one byte per row, one bit per column.
The two planes are overlayed to build the 2 bit tile.

## Registers
- `addr`: current VRAM address (15bit) (`v`)
- `temp_addr`: temporary VRAM address. used to prepare a value to be loaded into `addr` at a later point (`t`)
- `fine_x`: fine X scroll (`x`)
- `write_latch`: address write toggle latch for MMIO registers `0x2005` and `0x2006`, shared between the two (`w`)
- `data_latch`: less a latch and more wires with capacity

## Sources
- [nesdev: PPU scrolling](https://www.nesdev.org/wiki/PPU_scrolling)
