# Goals
First, a little bit about my goals with this emulator.

The most obvious question, why make it in the first place?
There are a lot of NES emulators out there, even a lot of accurate ones.
Short answer, because I want to.
It's gonna be fun (probably).
I wanted to write an emulator for a while, and the NES is a fairly simple system and I already had a decent knowledge about it.

Second question, why Rust?
Because I like the language and the ecosystem.
It should allow to build a fast emulator, that can run on many different platforms.
And the Rust type system will help a lot in ensuring the emulator run properly and correctly.
That being said, I am very new to Rust myself.
This is my first big Rust project, and will be a big learning experience, but that is part of the reason I chose Rust for this.
But it will mean I won't write the most perfect Rust code, so if you know how to improve it, tell me.
