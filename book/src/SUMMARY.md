# Summary

[Introduction](introduction.md)
# Implementation
- [Goals](implementation/goals.md)
- [CPU](implementation/cpu.md)
- [PPU](implementation/ppu.md)
