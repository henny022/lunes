use lunes::NES;
use lunes::cpu::Register;

#[test]
fn nestest() {
    // load test rom data
    let rom = std::fs::read("nestest.nes");
    assert!(
        rom.is_ok(),
        "could not load 'nestest.nes': {}\n\
        Please download both https://www.qmtpro.com/~nes/misc/nestest.nes \
        and https://www.qmtpro.com/~nes/misc/nestest.log.",
        rom.unwrap_err()
    );
    let mut rom = rom.unwrap();
    // patch entry point for automated test mode
    rom[0x400c] = 0;

    // load reference log
    let log = std::fs::read_to_string("nestest.log");
    assert!(
        log.is_ok(),
        "could not load 'nestest.log': {}\n\
        Please download both https://www.qmtpro.com/~nes/misc/nestest.nes \
        and https://www.qmtpro.com/~nes/misc/nestest.log.",
        log.unwrap_err()
    );
    let log = log.unwrap();
    // convert to common format "PC:XXXX  A:XX X:XX Y:XX P:XX SP:XX  CYC:XXXXX"
    let log = log.lines().map(|line| {
        let mut s = "PC:".to_owned();
        s += &line[0..4];
        s += "  ";
        s += &line[48..73];
        s += "  ";
        s += &line[86..];
        s
    });
    let mut nes = NES::from_bytes(&rom);

    let mut cycles: i32 = -1;
    for line in log {
        cycles += nes.step() as i32;
        let state = format!(
            "PC:{:04X}  A:{:02X} X:{:02X} Y:{:02X} P:{:02X} SP:{:02X}  CYC:{cycles}",
            nes.cpu.pc,
            nes.cpu.a,
            nes.cpu.x,
            nes.cpu.y,
            nes.cpu.read_reg(Register::P),
            nes.cpu.sp,
        );
        assert_eq!(state, line);
    }
}
