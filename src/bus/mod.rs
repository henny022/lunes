pub mod cpu;
pub mod ppu;

pub use cpu::CpuBus;
pub use ppu::PpuBus;

#[derive(Default)]
pub struct Bus {
    pub cpu: CpuBus,
    pub ppu: PpuBus,
}
