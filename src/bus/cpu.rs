#[derive(Default)]
pub struct CpuBus {
    pub addr: u16,
    pub data: u8,
    pub r_w: bool,
}

impl CpuBus {
    pub fn addr(&mut self, addr: u16, r_w: bool) {
        self.addr = addr;
        self.r_w = r_w;
    }

    pub fn write(&mut self, value: u8) {
        self.data = value;
    }

    pub fn read(&self) -> u8 {
        self.data
    }
}
