use crate::bus::Bus;

#[allow(dead_code)]
#[derive(Default)]
pub struct Ppu {
    pub addr: u16,
    pub temp_addr: u16,
    pub fine_x: u8,
    pub write_latch: AddressLatch,
    pub data_latch: u8,
    pub read_buffer: u8,

    pub nmi: bool,
    pub ext: Ext,
    pub sprite_size: SpriteSize,
    pub background_table: PatternTable,
    pub sprite_table: PatternTable,
    pub address_increment: AddressIncrement,

    // PPU_STATUS
    pub vblank: bool,
    pub sprite_0_hit: bool,
    pub sprite_overflow: bool,

    // PPU_MASK
    pub greyscale: bool,
    pub show_bg_border: bool,
    pub show_sprite_border: bool,
    pub show_bg: bool,
    pub show_sprite: bool,
    pub emphasis_red: bool,
    pub emphasis_green: bool,
    pub emphasis_blue: bool,

    pub resetting: bool,

    pub line: u32,
    pub dot: u32,
}

#[derive(Debug, Default, Copy, Clone)]
pub enum AddressIncrement {
    #[default]
    Increment1,
    Increment32,
}

impl From<bool> for AddressIncrement {
    fn from(value: bool) -> Self {
        if value {
            AddressIncrement::Increment32
        } else {
            AddressIncrement::Increment1
        }
    }
}

#[derive(Debug, Default)]
pub enum Ext {
    #[default]
    ExtIn,
    ExtOut,
}

impl From<bool> for Ext {
    fn from(value: bool) -> Self {
        if value {
            Ext::ExtOut
        } else {
            Ext::ExtIn
        }
    }
}

#[derive(Debug, Default)]
pub enum SpriteSize {
    #[default]
    Sprite8x8,
    Sprite8x16,
}

impl From<bool> for SpriteSize {
    fn from(value: bool) -> Self {
        if value {
            SpriteSize::Sprite8x16
        } else {
            SpriteSize::Sprite8x8
        }
    }
}

#[derive(Default)]
pub enum AddressLatch {
    #[default]
    High,
    Low,
}

impl AddressLatch {
    fn toggle(&mut self) {
        *self = match self {
            AddressLatch::Low => AddressLatch::High,
            AddressLatch::High => AddressLatch::Low,
        }
    }
}

#[derive(Debug, Default)]
pub enum PatternTable {
    #[default]
    PatternTable0 = 0x0000,
    PatternTable1 = 0x1000,
}

impl From<bool> for PatternTable {
    fn from(value: bool) -> Self {
        if value {
            PatternTable::PatternTable1
        } else {
            PatternTable::PatternTable0
        }
    }
}

#[derive(Default)]
pub enum Nametable {
    #[default]
    Nametable0 = 0x2000,
    Nametable1 = 0x2400,
    Nametable2 = 0x2800,
    Nametable3 = 0x2C00,
}

impl Ppu {
    pub fn tick(&mut self, bus: &mut Bus) {
        // TODO order of these steps
        // update CPU MMIO
        if (bus.cpu.addr & 0xe000) == 0x2000 {
            if bus.cpu.r_w {
                match bus.cpu.addr & 0xe007 {
                    0x2002 => {
                        self.write_latch = AddressLatch::High;
                        self.data_latch &= 0x1f;
                        if self.vblank {
                            // TODO false negative race condition
                            self.data_latch |= 0x80;
                            self.vblank = false;
                        }
                        if self.sprite_0_hit {
                            self.data_latch |= 0x40;
                        }
                        if self.sprite_overflow {
                            self.data_latch |= 0x20;
                        }
                    }
                    0x2004 => todo!("OAM_DATA read"),
                    0x2005 => todo!(),
                    0x2006 => todo!(),
                    0x2007 => todo!(),
                    _ => (),
                };
                bus.cpu.write(self.data_latch);
            } else {
                self.data_latch = bus.cpu.read();
                match bus.cpu.addr & 0xe007 {
                    0x2000 => if !self.resetting {
                        self.nmi = (self.data_latch & 0x80) != 0;
                        self.ext = ((self.data_latch & 0x40) != 0).into();
                        self.sprite_size = ((self.data_latch & 0x20) != 0).into();
                        self.background_table = ((self.data_latch & 0x10) != 0).into();
                        self.sprite_table = ((self.data_latch & 0x08) != 0).into();
                        self.address_increment = ((self.data_latch & 0x04) != 0).into();
                        let nametable = self.data_latch & 3;
                        self.temp_addr &= 0x73ff;
                        self.temp_addr |= (nametable as u16) << 10;
                    }
                    0x2001 => if !self.resetting {
                        self.emphasis_blue = (self.data_latch & 0x80) != 0;
                        // TODO PAL has green and red swapped
                        self.emphasis_green = (self.data_latch & 0x40) != 0;
                        self.emphasis_red = (self.data_latch & 0x20) != 0;
                        self.show_sprite = (self.data_latch & 0x10) != 0;
                        self.show_bg = (self.data_latch & 0x08) != 0;
                        self.show_sprite_border = (self.data_latch & 0x04) != 0;
                        self.show_bg_border = (self.data_latch & 0x02) != 0;
                        self.greyscale = (self.data_latch & 0x01) != 0;
                    },
                    0x2003 => todo!(),
                    0x2004 => todo!("OAM_DATA write"),
                    0x2005 => if !self.resetting {
                        match self.write_latch {
                            AddressLatch::High => {
                                self.fine_x = self.data_latch & 0x07;
                                self.temp_addr &= 0x7fe0;
                                self.temp_addr |= (self.data_latch >> 3) as u16;
                            }
                            AddressLatch::Low => {
                                self.temp_addr &= 0x0c1f;
                                self.temp_addr |= ((self.data_latch & 0x07) as u16) << 12;
                                self.temp_addr |= (((self.data_latch >> 3) & 0x07) as u16) << 5;
                                self.temp_addr |= (((self.data_latch >> 6) & 0x03) as u16) << 8;
                            }
                        }
                        self.write_latch.toggle();
                    },
                    0x2006 => if !self.resetting {
                        match self.write_latch {
                            AddressLatch::High => {
                                self.temp_addr &= 0x00ff;
                                self.temp_addr |= ((self.data_latch & 0x3f) as u16) << 8;
                            }
                            AddressLatch::Low => {
                                self.temp_addr &= 0x7f00;
                                self.temp_addr |= self.data_latch as u16;
                                self.addr = self.temp_addr;
                            }
                        }
                        self.write_latch.toggle();
                    },
                    0x2007 => {
                        // TODO ppu bus IO
                        self.addr += match self.address_increment {
                            AddressIncrement::Increment1 => 1,
                            AddressIncrement::Increment32 => 32,
                        };
                    }
                    _ => (),
                };
            }
        }
        // data fetches
        // rendering
        // sprite evaluation

        // set flags
        if self.line == 241 && self.dot == 1 {
            // TODO false negative race condition
            self.vblank = true;
        }
        if self.line == 261 && self.dot == 1 {
            self.vblank = false;
            self.sprite_0_hit = false;
            self.sprite_overflow = false;
            self.resetting = false;
        }
        // progress
        self.dot += 1;
        if self.dot > 340 {
            self.dot = 0;
            self.line += 1;
            if self.line > 261 {
                self.line = 0;
                // TODO odd frames
            }
        }
    }

    pub fn fine_y(&self) -> u16 {
        (self.addr >> 12) & 0x7
    }

    pub fn coarse_x(&self) -> u16 {
        self.addr & 0x1f
    }

    pub fn coarse_y(&self) -> u16 {
        (self.addr >> 5) & 0x1f
    }

    pub fn nametable(&self) -> Nametable {
        match self.addr & 0x0c00 {
            0x0000 => Nametable::Nametable0,
            0x0400 => Nametable::Nametable1,
            0x0800 => Nametable::Nametable2,
            0x0c00 => Nametable::Nametable3,
            _ => unreachable!()
        }
    }
}
