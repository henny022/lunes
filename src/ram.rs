use crate::bus::CpuBus;

pub struct Ram {
    content: [u8; 0x800],
}

impl Default for Ram {
    fn default() -> Self {
        Self {
            content: [0; 0x800],
        }
    }
}

impl Ram {
    pub fn tick(&mut self, bus: &mut CpuBus) {
        match bus.addr {
            0..=0x1fff => match bus.r_w {
                true => bus.write(self.content[(bus.addr & 0x7ff) as usize]),
                false => self.content[(bus.addr & 0x7ff) as usize] = bus.data,
            },
            _ => (),
        }
    }
}
