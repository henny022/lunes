use lunes::NES;
use std::any::Any;
use std::sync::Arc;
use wgpu::{BindGroup, Device, Queue, RenderPipeline, Surface, Texture};
use winit::application::ApplicationHandler;
use winit::event::WindowEvent;
use winit::event_loop::{ActiveEventLoop, ControlFlow, EventLoop};
use winit::window::{Window, WindowId};

struct RenderState {
    surface: Surface<'static>,
    queue: Queue,
    device: Device,
    pipeline: RenderPipeline,
    texture: Texture,
    bind_group: BindGroup,
}

#[derive(Default)]
enum App {
    #[default]
    Uninitialized,
    Initialized {
        window: Arc<Window>,
        render_state: RenderState,
    },
}

#[derive(Debug)]
enum LunesEvent {
    Frame(Vec<u8>),
    Panic(Box<dyn Any + Send>),
    CoreStopped,
}

impl ApplicationHandler<LunesEvent> for App {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let window = Arc::new(event_loop.create_window(Window::default_attributes()).unwrap());

        *self = App::Initialized {
            window: window.clone(),
            render_state: pollster::block_on(RenderState::new(window)),
        };
    }

    fn user_event(&mut self, _event_loop: &ActiveEventLoop, event: LunesEvent) {
        match self {
            App::Uninitialized => (),
            App::Initialized { render_state, .. } =>
                match event {
                    LunesEvent::Frame(data) => {
                        //TODO render frame
                        println!("frame");
                        render_state.render(&data);
                    }
                    LunesEvent::Panic(error) => {
                        if let Some(error) = error.downcast_ref::<String>() {
                            eprintln!("panic {error}");
                        } else if let Some(error) = error.downcast_ref::<&str>() {
                            eprintln!("panic {error}");
                        } else {
                            eprintln!("panic {error:?} {:?}", (*error).type_id());
                        }
                    }
                    LunesEvent::CoreStopped => {
                        println!("core stopped");
                    }
                }
        }
    }

    fn window_event(&mut self, event_loop: &ActiveEventLoop, _window_id: WindowId, event: WindowEvent) {
        match self {
            App::Uninitialized => (),
            App::Initialized { window, .. } =>
                match event {
                    WindowEvent::CloseRequested => {
                        event_loop.exit();
                    }
                    WindowEvent::RedrawRequested => {
                        window.request_redraw();
                    }
                    _ => ()
                }
        }
    }
}


impl RenderState {
    async fn new(window: Arc<Window>) -> Self {
        // TODO proper error handling
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::PRIMARY,
            ..Default::default()
        });
        let surface = instance.create_surface(window.clone()).unwrap();
        let adapter = instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: Default::default(),
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        }).await.unwrap();
        let (device, queue) = adapter.request_device(&wgpu::DeviceDescriptor {
            label: None,
            required_features: wgpu::Features::empty(),
            required_limits: Default::default(),
            memory_hints: Default::default(),
        }, None).await.unwrap();

        let size = window.inner_size();
        let surface_caps = surface.get_capabilities(&adapter);
        // Shader code in this tutorial assumes an sRGB surface texture. Using a different
        // one will result in all the colors coming out darker. If you want to support non
        // sRGB surfaces, you'll need to account for that when drawing to the frame.
        let surface_format = surface_caps.formats.iter()
            .find(|f| f.is_srgb())
            .copied()
            .unwrap_or(surface_caps.formats[0]);
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size.width,
            height: size.height,
            present_mode: surface_caps.present_modes[0],
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
            desired_maximum_frame_latency: 2,
        };
        surface.configure(&device, &config);

        let texture_size = wgpu::Extent3d {
            width: 256,
            height: 240,
            depth_or_array_layers: 1,
        };
        let texture = device.create_texture(
            &wgpu::TextureDescriptor {
                size: texture_size,
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Rgba8UnormSrgb,
                usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
                label: None,
                view_formats: &[],
            }
        );
        let texture_view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });
        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
                label: None,
            });
        let bind_group = device.create_bind_group(
            &wgpu::BindGroupDescriptor {
                layout: &texture_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::TextureView(&texture_view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::Sampler(&sampler),
                    }
                ],
                label: None,
            }
        );

        let shader = device.create_shader_module(wgpu::include_wgsl!("lunes.wgsl"));
        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: None,
                bind_group_layouts: &[&texture_bind_group_layout],
                push_constant_ranges: &[],
            });
        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: None,
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[],
                compilation_options: wgpu::PipelineCompilationOptions::default(),
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
                compilation_options: wgpu::PipelineCompilationOptions::default(),
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Cw,
                cull_mode: None,
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
            cache: None,
        });


        Self {
            surface,
            queue,
            device,
            pipeline,
            texture,
            bind_group,
        }
    }

    fn color_map(_: u8) -> [u8; 4] {
        // TODO proper palette decoding
        // do I even really want to do this here?
        [20, 20, 20, 255]
    }

    fn render(&self, data: &[u8]) {
        let texture_size = wgpu::Extent3d {
            width: 256,
            height: 240,
            depth_or_array_layers: 1,
        };
        let data: Vec<u8> = data.iter().copied().flat_map(Self::color_map).collect();
        self.queue.write_texture(
            wgpu::ImageCopyTexture {
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            &data,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * 256),
                rows_per_image: Some(240),
            },
            texture_size,
        );


        let output = self.surface.get_current_texture().unwrap();
        let view = output.texture.create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: None,
        });
        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[
                    Some(wgpu::RenderPassColorAttachment {
                        view: &view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Clear(
                                wgpu::Color {
                                    r: 0.1,
                                    g: 0.2,
                                    b: 0.3,
                                    a: 1.0,
                                }
                            ),
                            store: wgpu::StoreOp::Store,
                        },
                    })
                ],
                depth_stencil_attachment: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });
            render_pass.set_pipeline(&self.pipeline);
            render_pass.set_bind_group(0, &self.bind_group, &[]);
            render_pass.draw(0..4, 0..1);
        }

        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();
    }
}
fn main() {
    env_logger::init();

    let event_loop = EventLoop::<LunesEvent>::with_user_event().build().unwrap();
    event_loop.set_control_flow(ControlFlow::Poll);
    let mut app = App::default();

    let proxy = event_loop.create_proxy();
    let lunes_wrapper = std::thread::Builder::new().name("lunes wrapper".into()).spawn(move || {
        let proxy2 = proxy.clone();
        let lunes = std::thread::Builder::new().name("lunes".into()).spawn(move || {
            // load test rom data
            let rom = std::fs::read("ppu_read_buffer/test_ppu_read_buffer.nes");
            assert!(
                rom.is_ok(),
                "could not load rom: {}",
                rom.unwrap_err()
            );
            let rom = rom.unwrap();

            let mut nes = NES::from_bytes(&rom);

            let mut last_line = 0;
            loop {
                nes.step();
                if nes.ppu.line != last_line {
                    last_line = nes.ppu.line;
                    if last_line == 241 {
                        // TODO grab actual framebuffer
                        proxy.send_event(LunesEvent::Frame((0..(256 * 240)).map(|_| 0).collect())).unwrap();
                    }
                }
            }
        }).unwrap();
        match lunes.join() {
            Ok(_) => proxy2.send_event(LunesEvent::CoreStopped).unwrap(),
            Err(error) => proxy2.send_event(LunesEvent::Panic(error)).unwrap(),
        }
    }).unwrap();

    event_loop.run_app(&mut app).unwrap();
    lunes_wrapper.join().unwrap();
}
