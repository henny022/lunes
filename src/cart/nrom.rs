use crate::ines::INES;

use super::CartImpl;

#[allow(dead_code)]
pub struct NROM {
    prg: NromPrg,
    ram: NromRam,
    chr: NromChr,
}

enum NromPrg {
    Prg128([u8; 16 * 1024]),
    Prg256([u8; 32 * 1024]),
}

enum NromRam {
    None,
    // TODO use INES 2 for better loading
    // Ram2([u8; 2 * 1024]),
    // Ram4([u8; 4 * 1024]),
    Ram8([u8; 8 * 1024]),
}

#[allow(dead_code)]
struct NromChr([u8; 8 * 1024]);

impl NROM {
    pub(super) fn load(header: &INES, prg: &[u8], chr: &[u8]) -> Option<Self> {
        let prg = match header.prg_size {
            1 => NromPrg::Prg128(prg.try_into().ok()?),
            2 => NromPrg::Prg256(prg.try_into().ok()?),
            _ => return None,
        };
        let ram = match header.prg_ram {
            false => NromRam::None,
            true => match header.prg_ram_size {
                1 => NromRam::Ram8([0; 8 * 1024]),
                _ => return None,
            },
        };
        let chr = NromChr(chr.try_into().ok()?);
        Some(Self { prg, ram, chr })
    }
}

impl CartImpl for NROM {
    fn tick(&mut self, bus: &mut crate::bus::Bus) {
        match bus.cpu.addr {
            0x8000..=0xffff => {
                if bus.cpu.r_w {
                    match self.prg {
                        NromPrg::Prg128(data) => bus.cpu.write(data[(bus.cpu.addr & 0x3fff) as usize]),
                        NromPrg::Prg256(data) => bus.cpu.write(data[(bus.cpu.addr & 0x7fff) as usize]),
                    }
                }
            }
            0x6000..=0x7fff => match self.ram {
                NromRam::None => (),
                NromRam::Ram8(mut data) => match bus.cpu.r_w {
                    true => bus.cpu.write(data[(bus.cpu.addr & 0x1fff) as usize]),
                    false => data[(bus.cpu.addr & 0x1fff) as usize] = bus.cpu.data,
                },
            },
            _ => (),
        }
    }
}
