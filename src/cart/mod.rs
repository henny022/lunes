use crate::{bus::Bus, ines::INES};

mod nrom;

use nrom::NROM;

pub enum Cart {
    NROM(NROM),
}

impl Cart {
    pub fn tick(&mut self, bus: &mut Bus) {
        match self {
            Cart::NROM(nrom) => nrom.tick(bus),
        }
    }
}

trait CartImpl {
    fn tick(&mut self, bus: &mut Bus);
}

pub fn load(header: &INES, prg: &[u8], chr: &[u8]) -> Cart {
    match header.mapper {
        0 => Cart::NROM(NROM::load(header, prg, chr).expect("TODO")),
        3 => Cart::NROM(NROM::load(header, prg, &chr[..8192]).expect("TODO")),
        _ => todo!(),
    }
}

pub fn from_bytes(data: &[u8]) -> Cart {
    let header = INES::from_bytes(data).expect("TODO");
    let prg_start = 16usize;
    let prg_end = prg_start + header.prg_size as usize * 16 * 1024;
    let prg = &data[prg_start..prg_end];
    let chr_start = prg_end;
    let chr_end = chr_start + header.chr_size as usize * 8 * 1024;
    let chr = &data[chr_start..chr_end];
    load(&header, prg, chr)
}

pub fn from_file(filename: &str) -> Cart {
    let data = std::fs::read(filename).expect("TODO");
    from_bytes(&data)
}
