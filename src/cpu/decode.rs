use super::*;

impl Cpu {
    pub(super) fn decode(opcode: u8) -> CpuState {
        let op = match opcode {
            // BRK
            0x00 => ReadCycle::Interrupt(InterruptSource::BRK),
            // JSR address
            0x20 => ReadCycle::JSR,
            // RTI
            0x40 => ReadCycle::ThrowAway(StackOp::RTI),
            // RTS
            0x60 => ReadCycle::ThrowAway(StackOp::RTS),
            // PHP
            0x08 => ReadCycle::ThrowAway(StackOp::PHP),
            // PLP
            0x28 => ReadCycle::ThrowAway(StackOp::PLP),
            // PHA
            0x48 => ReadCycle::ThrowAway(StackOp::PHA),
            // PLA
            0x68 => ReadCycle::ThrowAway(StackOp::PLA),

            // CLC
            0x18 => ReadCycle::Implied(ImpliedOp::Clear(Flag::C)),
            // SEC
            0x38 => ReadCycle::Implied(ImpliedOp::Set(Flag::C)),
            // CLI
            0x58 => ReadCycle::Implied(ImpliedOp::Clear(Flag::I)),
            // SEI
            0x78 => ReadCycle::Implied(ImpliedOp::Set(Flag::I)),
            // CLV
            0xb8 => ReadCycle::Implied(ImpliedOp::Clear(Flag::V)),
            // CLD
            0xd8 => ReadCycle::Implied(ImpliedOp::Clear(Flag::D)),
            // SED
            0xf8 => ReadCycle::Implied(ImpliedOp::Set(Flag::D)),

            // BIT zero page
            0x24 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::BIT)),
            // BIT absolute
            0x2c => ReadCycle::Absolute(OpClass::Read(ReadOp::BIT)),

            // JMP absolute
            0x4c => ReadCycle::JMP(false),
            // JMP absolute indirect
            0x6c => ReadCycle::JMP(true),

            // ORA immediate
            0x09 => ReadCycle::Immediate(ReadOp::ORA),
            // ORA zero page
            0x05 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::ORA)),
            // ORA zero page indexed
            0x15 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::ORA)),
            // ORA absolute
            0x0d => ReadCycle::Absolute(OpClass::Read(ReadOp::ORA)),
            // ORA absolute indexed Y
            0x19 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::ORA)),
            // ORA absolute indexed X
            0x1d => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::ORA)),
            // ORA indirect indexed
            0x11 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::ORA)),
            // ORA indexed indirect
            0x01 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::ORA)),

            // AND immediate
            0x29 => ReadCycle::Immediate(ReadOp::AND),
            // AND zero page
            0x25 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::AND)),
            // AND zero page indexed
            0x35 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::AND)),
            // AND absolute
            0x2d => ReadCycle::Absolute(OpClass::Read(ReadOp::AND)),
            // AND absolute indexed Y
            0x39 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::AND)),
            // AND absolute indexed X
            0x3d => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::AND)),
            // AND indirect indexed
            0x31 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::AND)),
            // AND indexed indirect
            0x21 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::AND)),

            // EOR immediate
            0x49 => ReadCycle::Immediate(ReadOp::EOR),
            // EOR zero page
            0x45 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::EOR)),
            // EOR zero page indexed
            0x55 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::EOR)),
            // EOR absolute
            0x4d => ReadCycle::Absolute(OpClass::Read(ReadOp::EOR)),
            // EOR absolute indexed Y
            0x59 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::EOR)),
            // EOR absolute indexed X
            0x5d => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::EOR)),
            // EOR indirect indexed
            0x51 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::EOR)),
            // EOR indexed indirect
            0x41 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::EOR)),

            // ADC immediate
            0x69 => ReadCycle::Immediate(ReadOp::ADC),
            // ADC zero page
            0x65 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::ADC)),
            // ADC zero page indexed
            0x75 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::ADC)),
            // ADC absolute
            0x6d => ReadCycle::Absolute(OpClass::Read(ReadOp::ADC)),
            // ADC absolute indexed Y
            0x79 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::ADC)),
            // ADC absolute indexed X
            0x7d => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::ADC)),
            // ADC indirect indexed
            0x71 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::ADC)),
            // ADC indexed indirect
            0x61 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::ADC)),

            // (unofficial) NOP immediate (STA immediate)
            0x89 => ReadCycle::Immediate(ReadOp::Nop),
            // STA zero page
            0x85 => ReadCycle::ZeroPage(OpClass::Write(Store(Register::A))),
            // STA zero page indexed
            0x95 => ReadCycle::ZeroPageIndexed(OpClass::Write(Store(Register::A))),
            // STA absolute
            0x8d => ReadCycle::Absolute(OpClass::Write(Store(Register::A))),
            // STA absolute indexed Y
            0x99 => ReadCycle::AbsoluteIndexedY(OpClass::Write(Store(Register::A))),
            // STA absolute indexed X
            0x9d => ReadCycle::AbsoluteIndexedX(OpClass::Write(Store(Register::A))),
            // STA indirect indexed
            0x91 => ReadCycle::IndirectIndexed(OpClass::Write(Store(Register::A))),
            // STA indexed indirect
            0x81 => ReadCycle::IndexedIndirect(OpClass::Write(Store(Register::A))),

            // (unofficial) NOP immediate (STY immediate)
            0x80 => ReadCycle::Immediate(ReadOp::Nop),
            // STY zero page
            0x84 => ReadCycle::ZeroPage(OpClass::Write(Store(Register::Y))),
            // STY zero page indexed
            0x94 => ReadCycle::ZeroPageIndexed(OpClass::Write(Store(Register::Y))),
            // STY absolute
            0x8c => ReadCycle::Absolute(OpClass::Write(Store(Register::Y))),
            // (unofficial) SHY absolute indexed X TODO
            0x9c => ReadCycle::AbsoluteIndexedX(OpClass::Write(Store(Register::Y))),

            // (unofficial) NOP immediate (STX immediate)
            0x82 => ReadCycle::Immediate(ReadOp::Nop),
            // STX zero page
            0x86 => ReadCycle::ZeroPage(OpClass::Write(Store(Register::X))),
            // STX zero page indexed
            0x96 => ReadCycle::ZeroPageIndexedY(OpClass::Write(Store(Register::X))),
            // STX absolute
            0x8e => ReadCycle::Absolute(OpClass::Write(Store(Register::X))),
            // (unofficial) SHX absolute indexed Y TODO
            0x9e => ReadCycle::AbsoluteIndexedY(OpClass::Write(Store(Register::X))),

            // LDA immediate
            0xa9 => ReadCycle::Immediate(ReadOp::Load(Register::A)),
            // LDA zero page
            0xa5 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA zero page indexed
            0xb5 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA absolute
            0xad => ReadCycle::Absolute(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA absolute indexed Y
            0xb9 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA absolute indexed X
            0xbd => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA indirect indexed
            0xb1 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::Load(Register::A))),
            // LDA indexed indirect
            0xa1 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::Load(Register::A))),

            // LDY immediate
            0xa0 => ReadCycle::Immediate(ReadOp::Load(Register::Y)),
            // LDY zero page
            0xa4 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Load(Register::Y))),
            // LDY zero page indexed
            0xb4 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Load(Register::Y))),
            // LDY absolute
            0xac => ReadCycle::Absolute(OpClass::Read(ReadOp::Load(Register::Y))),
            // LDY absolute indexed X
            0xbc => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Load(Register::Y))),

            // LDX immediate
            0xa2 => ReadCycle::Immediate(ReadOp::Load(Register::X)),
            // LDX zero page
            0xa6 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Load(Register::X))),
            // LDX zero page indexed
            0xb6 => ReadCycle::ZeroPageIndexedY(OpClass::Read(ReadOp::Load(Register::X))),
            // LDX absolute
            0xae => ReadCycle::Absolute(OpClass::Read(ReadOp::Load(Register::X))),
            // LDX absolute indexed Y
            0xbe => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::Load(Register::X))),

            // (unofficial) LAX immediate
            0xab => ReadCycle::Immediate(ReadOp::Load(Register::AX)),
            // (unofficial) LAX zero page
            0xa7 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Load(Register::AX))),
            // (unofficial) LAX zero page indexed
            0xb7 => ReadCycle::ZeroPageIndexedY(OpClass::Read(ReadOp::Load(Register::AX))),
            // (unofficial) LAX absolute
            0xaf => ReadCycle::Absolute(OpClass::Read(ReadOp::Load(Register::AX))),
            // (unofficial) LAX absolute indexed Y
            0xbf => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::Load(Register::AX))),
            // (unofficial) LAX indirect indexed
            0xb3 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::Load(Register::AX))),
            // (unofficial) LAX indexed indirect
            0xa3 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::Load(Register::AX))),

            // (unofficial) SAX zero page
            0x87 => ReadCycle::ZeroPage(OpClass::Write(Store(Register::AX))),
            // (unofficial) SAX zero page indexed
            0x97 => ReadCycle::ZeroPageIndexedY(OpClass::Write(Store(Register::AX))),
            // (unofficial) SAX absolute
            0x8f => ReadCycle::Absolute(OpClass::Write(Store(Register::AX))),
            // (unofficial) SAX indexed indirect
            0x83 => ReadCycle::IndexedIndirect(OpClass::Write(Store(Register::AX))),

            // CMP immediate
            0xc9 => ReadCycle::Immediate(ReadOp::Compare(Register::A)),
            // CMP zero page
            0xc5 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP zero page indexed
            0xd5 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP absolute
            0xcd => ReadCycle::Absolute(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP absolute indexed Y
            0xd9 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP absolute indexed X
            0xdd => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP indirect indexed
            0xd1 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::Compare(Register::A))),
            // CMP indexed indirect
            0xc1 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::Compare(Register::A))),

            // CPY immediate
            0xc0 => ReadCycle::Immediate(ReadOp::Compare(Register::Y)),
            // CPY zero page
            0xc4 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Compare(Register::Y))),
            // CPY absolute
            0xcc => ReadCycle::Absolute(OpClass::Read(ReadOp::Compare(Register::Y))),

            // CPX immediate
            0xe0 => ReadCycle::Immediate(ReadOp::Compare(Register::X)),
            // CPX zero page
            0xe4 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Compare(Register::X))),
            // CPX absolute
            0xec => ReadCycle::Absolute(OpClass::Read(ReadOp::Compare(Register::X))),

            // SBC immediate
            0xe9 => ReadCycle::Immediate(ReadOp::SBC),
            // (unofficial) SBC immediate
            0xeb => ReadCycle::Immediate(ReadOp::SBC),
            // SBC zero page
            0xe5 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::SBC)),
            // SBC zero page indexed
            0xf5 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::SBC)),
            // SBC absolute
            0xed => ReadCycle::Absolute(OpClass::Read(ReadOp::SBC)),
            // SBC absolute indexed Y
            0xf9 => ReadCycle::AbsoluteIndexedY(OpClass::Read(ReadOp::SBC)),
            // SBC absolute indexed X
            0xfd => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::SBC)),
            // SBC indirect indexed
            0xf1 => ReadCycle::IndirectIndexed(OpClass::Read(ReadOp::SBC)),
            // SBC indexed indirect
            0xe1 => ReadCycle::IndexedIndirect(OpClass::Read(ReadOp::SBC)),

            // TYA
            0x98 => ReadCycle::Implied(ImpliedOp::Transfer(Register::Y, Register::A)),
            // TAY
            0xa8 => ReadCycle::Implied(ImpliedOp::Transfer(Register::A, Register::Y)),
            // TXA
            0x8a => ReadCycle::Implied(ImpliedOp::Transfer(Register::X, Register::A)),
            // TAX
            0xaa => ReadCycle::Implied(ImpliedOp::Transfer(Register::A, Register::X)),
            // TXS
            0x9a => ReadCycle::Implied(ImpliedOp::Transfer(Register::X, Register::S)),
            // TSX
            0xba => ReadCycle::Implied(ImpliedOp::Transfer(Register::S, Register::X)),

            // INY
            0xc8 => ReadCycle::Implied(ImpliedOp::Modify(Register::Y, ReadModifyWriteOp::INC)),
            // INX
            0xe8 => ReadCycle::Implied(ImpliedOp::Modify(Register::X, ReadModifyWriteOp::INC)),

            // DEY
            0x88 => ReadCycle::Implied(ImpliedOp::Modify(Register::Y, ReadModifyWriteOp::DEC)),
            // DEX
            0xca => ReadCycle::Implied(ImpliedOp::Modify(Register::X, ReadModifyWriteOp::DEC)),

            // ASL
            0x0a => ReadCycle::Implied(ImpliedOp::Modify(Register::A, ReadModifyWriteOp::ASL)),
            // ASL zero page
            0x06 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::ASL, None)),
            // ASL zero page indexed
            0x16 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::ASL, None))
            }
            // ASL absolute
            0x0e => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::ASL, None)),
            // ASL absolute indexed
            0x1e => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::ASL, None))
            }

            // ROL
            0x2a => ReadCycle::Implied(ImpliedOp::Modify(Register::A, ReadModifyWriteOp::ROL)),
            // ROL zero page
            0x26 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROL, None)),
            // ROL zero page indexed
            0x36 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROL, None))
            }
            // ROL absolute
            0x2e => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROL, None)),
            // ROL absolute indexed
            0x3e => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROL, None))
            }

            // LSR
            0x4a => ReadCycle::Implied(ImpliedOp::Modify(Register::A, ReadModifyWriteOp::LSR)),
            // LSR zero page
            0x46 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::LSR, None)),
            // LSR zero page indexed
            0x56 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::LSR, None))
            }
            // LSR absolute
            0x4e => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::LSR, None)),
            // LSR absolute indexed
            0x5e => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::LSR, None))
            }

            // ROR
            0x6a => ReadCycle::Implied(ImpliedOp::Modify(Register::A, ReadModifyWriteOp::ROR)),
            // ROR zero page
            0x66 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROR, None)),
            // ROR zero page indexed
            0x76 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROR, None))
            }
            // ROR absolute
            0x6e => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROR, None)),
            // ROR absolute indexed
            0x7e => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::ROR, None))
            }

            // DEC zero page
            0xc6 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::DEC, None)),
            // DEC zero page indexed
            0xd6 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::DEC, None))
            }
            // DEC absolute
            0xce => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::DEC, None)),
            // DEC absolute indexed
            0xde => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::DEC, None))
            }
            // INC zero page
            0xe6 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(ReadModifyWriteOp::INC, None)),
            // INC zero page indexed
            0xf6 => {
                ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(ReadModifyWriteOp::INC, None))
            }
            // INC absolute
            0xee => ReadCycle::Absolute(OpClass::ReadModifyWrite(ReadModifyWriteOp::INC, None)),
            // INC absolute indexed
            0xfe => {
                ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(ReadModifyWriteOp::INC, None))
            }

            // SLO zero page
            0x07 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO zero page indexed
            0x17 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO absolute
            0x0f => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO absolute indexed X
            0x1f => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO absolute indexed Y
            0x1b => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO indexed indirect
            0x03 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),
            // SLO indirect indexed
            0x13 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ASL,
                Some(ReadOp::ORA),
            )),

            // RLA zero page
            0x27 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA zero page indexed
            0x37 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA absolute
            0x2f => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA absolute indexed X
            0x3f => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA absolute indexed Y
            0x3b => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA indexed indirect
            0x23 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),
            // RLA indirect indexed
            0x33 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROL,
                Some(ReadOp::AND),
            )),

            // SRE zero page
            0x47 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE zero page indexed
            0x57 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE absolute
            0x4f => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE absolute indexed X
            0x5f => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE absolute indexed Y
            0x5b => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE indexed indirect
            0x43 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),
            // SRE indirect indexed
            0x53 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::LSR,
                Some(ReadOp::EOR),
            )),

            // RRA zero page
            0x67 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA zero page indexed
            0x77 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA absolute
            0x6f => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA absolute indexed X
            0x7f => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA absolute indexed Y
            0x7b => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA indexed indirect
            0x63 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),
            // RRA indirect indexed
            0x73 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::ROR,
                Some(ReadOp::ADC),
            )),

            // DCP zero page
            0xc7 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP zero page indexed
            0xd7 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP absolute
            0xcf => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP absolute indexed X
            0xdf => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP absolute indexed Y
            0xdb => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP indexed indirect
            0xc3 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),
            // DCP indirect indexed
            0xd3 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::DEC,
                Some(ReadOp::Compare(Register::A)),
            )),

            // ISC zero page
            0xe7 => ReadCycle::ZeroPage(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC zero page indexed
            0xf7 => ReadCycle::ZeroPageIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC absolute
            0xef => ReadCycle::Absolute(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC absolute indexed X
            0xff => ReadCycle::AbsoluteIndexedX(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC absolute indexed Y
            0xfb => ReadCycle::AbsoluteIndexedY(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC indexed indirect
            0xe3 => ReadCycle::IndexedIndirect(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),
            // ISC indirect indexed
            0xf3 => ReadCycle::IndirectIndexed(OpClass::ReadModifyWrite(
                ReadModifyWriteOp::INC,
                Some(ReadOp::SBC),
            )),

            // BPL
            0x10 => ReadCycle::Branch(Condition::PL),
            // BMI
            0x30 => ReadCycle::Branch(Condition::MI),
            // BVC
            0x50 => ReadCycle::Branch(Condition::VC),
            // BVS
            0x70 => ReadCycle::Branch(Condition::VS),
            // BCC
            0x90 => ReadCycle::Branch(Condition::CC),
            // BCS
            0xb0 => ReadCycle::Branch(Condition::CS),
            // BNE
            0xd0 => ReadCycle::Branch(Condition::NE),
            // BEQ
            0xf0 => ReadCycle::Branch(Condition::EQ),

            // STP
            0x02 => todo!("STP"),
            0x22 => todo!("STP"),
            0x42 => todo!("STP"),
            0x62 => todo!("STP"),
            0x12 => todo!("STP"),
            0x32 => todo!("STP"),
            0x52 => todo!("STP"),
            0x72 => todo!("STP"),
            0x92 => todo!("STP"),
            0xb2 => todo!("STP"),
            0xd2 => todo!("STP"),
            0xf2 => todo!("STP"),

            // NOP implied
            0xea => ReadCycle::Implied(ImpliedOp::Nop),
            // (unofficial) NOP implied
            0x1a => ReadCycle::Implied(ImpliedOp::Nop),
            0x3a => ReadCycle::Implied(ImpliedOp::Nop),
            0x5a => ReadCycle::Implied(ImpliedOp::Nop),
            0x7a => ReadCycle::Implied(ImpliedOp::Nop),
            0xda => ReadCycle::Implied(ImpliedOp::Nop),
            0xfa => ReadCycle::Implied(ImpliedOp::Nop),
            // (unofficial) NOP immediate
            0xc2 => ReadCycle::Immediate(ReadOp::Nop),
            0xe2 => ReadCycle::Immediate(ReadOp::Nop),
            // (unofficial) NOP zero page
            0x04 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Nop)),
            0x44 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Nop)),
            0x64 => ReadCycle::ZeroPage(OpClass::Read(ReadOp::Nop)),
            // (unofficial) NOP zero page indexed
            0x14 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            0x34 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            0x54 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            0x74 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            0xd4 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            0xf4 => ReadCycle::ZeroPageIndexed(OpClass::Read(ReadOp::Nop)),
            // (unofficial) NOP absolute indexed
            0x1c => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            0x3c => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            0x5c => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            0x7c => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            0xdc => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            0xfc => ReadCycle::AbsoluteIndexedX(OpClass::Read(ReadOp::Nop)),
            // (unofficial) NOP absolute
            0x0c => ReadCycle::Absolute(OpClass::Read(ReadOp::Nop)),

            _ => unimplemented!("opcode {opcode:02X}"),
        };
        CpuState::ReadCycle(op)
    }
}
