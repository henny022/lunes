use std::ops::{Shl, Shr};

use crate::bus::CpuBus;

mod decode;

#[derive(Debug)]
pub struct Cpu {
    pub a: u8,
    pub x: u8,
    pub y: u8,
    pub sp: u8,
    pub pc: u16,

    pub n: bool,
    pub z: bool,
    pub c: bool,
    pub v: bool,
    pub i: bool,
    pub d: bool,

    state: CpuState,
}

impl Default for Cpu {
    fn default() -> Self {
        Self {
            a: 0,
            x: 0,
            y: 0,
            sp: 0,
            pc: 0,
            n: false,
            z: false,
            c: false,
            v: false,
            i: true,
            d: false,
            state: CpuState::ReadCycle(ReadCycle::Reset),
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum CpuState {
    ReadCycle(ReadCycle),
    WriteCycle(WriteCycle),
}

#[derive(Debug, Clone, Copy)]
enum ReadCycle {
    FetchInstruction,
    Implied(ImpliedOp),
    Immediate(ReadOp),
    ZeroPage(OpClass),
    ZeroPageIndexed(OpClass),
    ZeroPageIndexed1(u8, OpClass),
    ZeroPageIndexedY(OpClass),
    ZeroPageIndexedY1(u8, OpClass),
    Absolute(OpClass),
    Absolute1(u8, OpClass),
    AbsoluteIndexedX(OpClass),
    AbsoluteIndexedY(OpClass),
    AbsoluteIndexedX1(u8, OpClass),
    AbsoluteIndexedY1(u8, OpClass),
    AbsoluteIndexed2(u8, u8, bool, OpClass),
    JMP(bool),
    JMP1(bool, u8),
    JMP2(u8, u8),
    JMP3(u8, u8, u8),
    IndexedIndirect(OpClass),
    IndexedIndirect1(u8, OpClass),
    IndexedIndirect2(u8, OpClass),
    IndexedIndirect3(u8, u8, OpClass),
    IndirectIndexed(OpClass),
    IndirectIndexed1(u8, OpClass),
    IndirectIndexed2(u8, u8, OpClass),
    IndirectIndexed3(u8, u8, bool, OpClass),
    ReadModifyWrite(u16, ReadModifyWriteOp, Option<ReadOp>),
    Read(u16, ReadOp),
    Branch(Condition),
    Branch1(i8),
    Branch2,
    ThrowAway(StackOp),
    RTI1,
    RTI2,
    RTI3,
    RTI4(u8),
    RTS1,
    RTS2,
    RTS3(u8),
    RTS4,
    PLA1,
    PLA2,
    PLP1,
    PLP2,
    Interrupt(InterruptSource),
    Interrupt4(InterruptSource),
    Interrupt5(InterruptSource, u8),
    JSR,
    JSR1(u8),
    JSR4(u8),
    Reset,
    Reset1,
}

#[derive(Debug, Clone, Copy)]
enum WriteCycle {
    ReadModifyWrite1(u16, u8, ReadModifyWriteOp, Option<ReadOp>),
    Write(u16, u8),
    PHA,
    PHP,
    Interrupt1(InterruptSource),
    Interrupt2(InterruptSource),
    Interrupt3(InterruptSource),
    JSR2(u8),
    JSR3(u8),
}

#[derive(Debug, Clone, Copy)]
enum ImpliedOp {
    Nop,
    Modify(Register, ReadModifyWriteOp),
    Transfer(Register, Register),
    Set(Flag),
    Clear(Flag),
}

#[derive(Debug, Clone, Copy)]
enum OpClass {
    Read(ReadOp),
    ReadModifyWrite(ReadModifyWriteOp, Option<ReadOp>),
    Write(Store),
}

#[derive(Debug, Clone, Copy)]
enum ReadOp {
    Nop,
    Load(Register),
    EOR,
    AND,
    ORA,
    ADC,
    SBC,
    Compare(Register),
    BIT,
}

#[derive(Debug, Clone, Copy)]
enum ReadModifyWriteOp {
    ASL,
    ROL,
    LSR,
    ROR,
    DEC,
    INC,
}

#[derive(Debug, Clone, Copy)]
struct Store(Register);

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Register {
    A,
    X,
    Y,
    AX,
    P,
    S,
}

#[derive(Debug, Clone, Copy)]
enum Condition {
    PL,
    MI,
    VC,
    VS,
    CC,
    CS,
    NE,
    EQ,
}

#[derive(Debug, Clone, Copy)]
enum Flag {
    C,
    V,
    I,
    D,
}

#[derive(Debug, Clone, Copy)]
enum StackOp {
    RTI,
    RTS,
    PHA,
    PHP,
    PLA,
    PLP,
}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq)]
enum InterruptSource {
    BRK,
    IRQ,
    NMI,
    RESET,
}

impl Cpu {
    pub fn phi1(&mut self, bus: &mut CpuBus) {
        match self.state {
            CpuState::ReadCycle(op) => {
                let addr = match op {
                    ReadCycle::FetchInstruction => self.pc_inc(),
                    ReadCycle::Implied(_) => self.pc,
                    ReadCycle::Immediate(_) => self.pc_inc(),
                    ReadCycle::ZeroPage(_) => self.pc_inc(),
                    ReadCycle::ZeroPageIndexed(_) => self.pc_inc(),
                    ReadCycle::ZeroPageIndexed1(addr, _) => addr as u16,
                    ReadCycle::ZeroPageIndexedY(_) => self.pc_inc(),
                    ReadCycle::ZeroPageIndexedY1(addr, _) => addr as u16,
                    ReadCycle::Absolute(_) => self.pc_inc(),
                    ReadCycle::Absolute1(_, _) => self.pc_inc(),
                    ReadCycle::AbsoluteIndexedX(_) => self.pc_inc(),
                    ReadCycle::AbsoluteIndexedY(_) => self.pc_inc(),
                    ReadCycle::AbsoluteIndexedX1(_, _) => self.pc_inc(),
                    ReadCycle::AbsoluteIndexedY1(_, _) => self.pc_inc(),
                    ReadCycle::AbsoluteIndexed2(lo, hi, _, _) => u16::from_le_bytes([lo, hi]),
                    ReadCycle::JMP(_) => self.pc_inc(),
                    ReadCycle::JMP1(_, _) => self.pc_inc(),
                    ReadCycle::JMP2(lo, hi) => u16::from_le_bytes([lo, hi]),
                    ReadCycle::JMP3(lo, hi, _) => u16::from_le_bytes([lo.wrapping_add(1), hi]),
                    ReadCycle::IndexedIndirect(_) => self.pc_inc(),
                    ReadCycle::IndexedIndirect1(poin, _) => poin as u16,
                    ReadCycle::IndexedIndirect2(poin, _) => poin as u16,
                    ReadCycle::IndexedIndirect3(poin, _, _) => poin.wrapping_add(1) as u16,
                    ReadCycle::IndirectIndexed(_) => self.pc_inc(),
                    ReadCycle::IndirectIndexed1(poin, _) => poin as u16,
                    ReadCycle::IndirectIndexed2(poin, _, _) => poin.wrapping_add(1) as u16,
                    ReadCycle::IndirectIndexed3(lo, hi, _, _) => u16::from_le_bytes([lo, hi]),
                    ReadCycle::ReadModifyWrite(addr, _, _) => addr,
                    ReadCycle::Read(addr, _) => addr,
                    ReadCycle::Branch(_) => self.pc_inc(),
                    ReadCycle::Branch1(_) => self.pc,
                    ReadCycle::Branch2 => self.pc,
                    ReadCycle::ThrowAway(_) => self.pc,
                    ReadCycle::RTI1 => self.stack_inc(),
                    ReadCycle::RTI2 => self.stack_inc(),
                    ReadCycle::RTI3 => self.stack_inc(),
                    ReadCycle::RTI4(_) => self.stack(),
                    ReadCycle::RTS1 => self.stack_inc(),
                    ReadCycle::RTS2 => self.stack_inc(),
                    ReadCycle::RTS3(_) => self.stack(),
                    ReadCycle::RTS4 => self.pc_inc(),
                    ReadCycle::PLA1 => self.stack_inc(),
                    ReadCycle::PLA2 => self.stack(),
                    ReadCycle::PLP1 => self.stack_inc(),
                    ReadCycle::PLP2 => self.stack(),
                    ReadCycle::Interrupt(_) => self.pc_inc(),
                    ReadCycle::Interrupt4(source) => match source {
                        InterruptSource::BRK => 0xfffe,
                        InterruptSource::IRQ => 0xfffe,
                        InterruptSource::NMI => 0xfffa,
                        InterruptSource::RESET => 0xfffc,
                    },
                    ReadCycle::Interrupt5(source, _) => match source {
                        InterruptSource::BRK => 0xffff,
                        InterruptSource::IRQ => 0xffff,
                        InterruptSource::NMI => 0xfffb,
                        InterruptSource::RESET => 0xfffd,
                    },
                    ReadCycle::JSR => self.pc_inc(),
                    ReadCycle::JSR1(_) => self.stack(),
                    ReadCycle::JSR4(_) => self.pc,
                    ReadCycle::Reset => self.pc,
                    ReadCycle::Reset1 => self.pc,
                };
                bus.addr(addr, true);
            }
            CpuState::WriteCycle(op) => {
                let (addr, value) = match op {
                    WriteCycle::ReadModifyWrite1(addr, v, _, _) => (addr, v),
                    WriteCycle::Write(addr, v) => (addr, v),
                    WriteCycle::PHA => (self.stack_dec(), self.a),
                    WriteCycle::PHP => (self.stack_dec(), self.read_reg(Register::P) | (1 << 4)),
                    WriteCycle::Interrupt1(source) => {
                        if source == InterruptSource::RESET {
                            bus.addr(self.stack_dec(), true);
                            return;
                        }
                        (self.stack_dec(), self.pc.to_le_bytes()[1])
                    }
                    WriteCycle::Interrupt2(source) => {
                        if source == InterruptSource::RESET {
                            bus.addr(self.stack_dec(), true);
                            return;
                        }
                        (self.stack_dec(), self.pc.to_le_bytes()[0])
                    }
                    WriteCycle::Interrupt3(source) => {
                        let mut p = self.read_reg(Register::P);
                        p = match source {
                            InterruptSource::BRK => p | (1 << 4),
                            InterruptSource::IRQ => p,
                            InterruptSource::NMI => p,
                            InterruptSource::RESET => {
                                bus.addr(self.stack_dec(), true);
                                return;
                            }
                        };
                        (self.stack_dec(), p)
                    }
                    WriteCycle::JSR2(_) => (self.stack_dec(), self.pc.to_le_bytes()[1]),
                    WriteCycle::JSR3(_) => (self.stack_dec(), self.pc.to_le_bytes()[0]),
                };
                bus.addr(addr, false);
                bus.write(value);
            }
        }
    }

    pub fn phi2(&mut self, bus: &CpuBus) {
        self.state = match self.state {
            CpuState::ReadCycle(op) => {
                let value = bus.read();
                match op {
                    ReadCycle::FetchInstruction => Cpu::decode(value),
                    ReadCycle::Implied(op) => {
                        match op {
                            ImpliedOp::Nop => (),
                            ImpliedOp::Modify(reg, op) => {
                                let mut v = self.read_reg(reg);
                                v = self.rmw_op(v, op);
                                self.write_reg(reg, v)
                            }
                            ImpliedOp::Transfer(from, to) => {
                                let v = self.read_reg(from);
                                if to != Register::S {
                                    self.set_n(v);
                                    self.set_z(v);
                                }
                                self.write_reg(to, v)
                            }
                            ImpliedOp::Set(flag) => match flag {
                                Flag::C => self.c = true,
                                Flag::V => self.v = true,
                                Flag::I => self.i = true,
                                Flag::D => self.d = true,
                            },
                            ImpliedOp::Clear(flag) => match flag {
                                Flag::C => self.c = false,
                                Flag::V => self.v = false,
                                Flag::I => self.i = false,
                                Flag::D => self.d = false,
                            },
                        };
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::Immediate(op) => self.read_op(op, value),
                    ReadCycle::ZeroPage(op) => {
                        let addr = value as u16;
                        match op {
                            OpClass::Read(op) => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::ZeroPageIndexed(op) => {
                        CpuState::ReadCycle(ReadCycle::ZeroPageIndexed1(value, op))
                    }
                    ReadCycle::ZeroPageIndexed1(addr, op) => {
                        let lo = addr;
                        let lo = lo.wrapping_add(self.x);
                        let addr = lo as u16;
                        match op {
                            OpClass::Read(op) => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::ZeroPageIndexedY(op) => {
                        CpuState::ReadCycle(ReadCycle::ZeroPageIndexedY1(value, op))
                    }
                    ReadCycle::ZeroPageIndexedY1(addr, op) => {
                        let lo = addr;
                        let lo = lo.wrapping_add(self.y);
                        let addr = lo as u16;
                        match op {
                            OpClass::Read(op) => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::Absolute(op) => CpuState::ReadCycle(ReadCycle::Absolute1(value, op)),
                    ReadCycle::Absolute1(lo, op) => {
                        let hi = value;
                        let addr = u16::from_le_bytes([lo, hi]);
                        match op {
                            OpClass::Read(op) => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::AbsoluteIndexedX(op) => {
                        CpuState::ReadCycle(ReadCycle::AbsoluteIndexedX1(value, op))
                    }
                    ReadCycle::AbsoluteIndexedY(op) => {
                        CpuState::ReadCycle(ReadCycle::AbsoluteIndexedY1(value, op))
                    }
                    ReadCycle::AbsoluteIndexedX1(lo, op) => {
                        let (lo, carry) = lo.overflowing_add(self.x);
                        CpuState::ReadCycle(ReadCycle::AbsoluteIndexed2(lo, value, carry, op))
                    }
                    ReadCycle::AbsoluteIndexedY1(lo, op) => {
                        let (lo, carry) = lo.overflowing_add(self.y);
                        CpuState::ReadCycle(ReadCycle::AbsoluteIndexed2(lo, value, carry, op))
                    }
                    ReadCycle::AbsoluteIndexed2(lo, hi, carry, op) => {
                        // TODO is wrapping correct here
                        let hi = hi.wrapping_add(carry as u8);
                        let addr = u16::from_le_bytes([lo, hi]);
                        let v = value;
                        match op {
                            OpClass::Read(op) => match carry {
                                true => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                                false => self.read_op(op, v),
                            },
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::JMP(indirect) => {
                        CpuState::ReadCycle(ReadCycle::JMP1(indirect, value))
                    }
                    ReadCycle::JMP1(indirect, lo) => {
                        if indirect {
                            CpuState::ReadCycle(ReadCycle::JMP2(lo, value))
                        } else {
                            let hi = value;
                            self.pc = u16::from_le_bytes([lo, hi]);
                            CpuState::ReadCycle(ReadCycle::FetchInstruction)
                        }
                    }
                    ReadCycle::JMP2(lo, hi) => CpuState::ReadCycle(ReadCycle::JMP3(lo, hi, value)),
                    ReadCycle::JMP3(_, _, lo) => {
                        let hi = value;
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::IndexedIndirect(op) => {
                        CpuState::ReadCycle(ReadCycle::IndexedIndirect1(value, op))
                    }
                    ReadCycle::IndexedIndirect1(poin, op) => {
                        let _ = value;
                        CpuState::ReadCycle(ReadCycle::IndexedIndirect2(
                            poin.wrapping_add(self.x),
                            op,
                        ))
                    }
                    ReadCycle::IndexedIndirect2(poin, op) => {
                        CpuState::ReadCycle(ReadCycle::IndexedIndirect3(poin, value, op))
                    }
                    ReadCycle::IndexedIndirect3(_, lo, op) => {
                        let hi = value;
                        let addr = u16::from_le_bytes([lo, hi]);
                        match op {
                            OpClass::Read(op) => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::IndirectIndexed(op) => {
                        CpuState::ReadCycle(ReadCycle::IndirectIndexed1(value, op))
                    }
                    ReadCycle::IndirectIndexed1(poin, op) => {
                        CpuState::ReadCycle(ReadCycle::IndirectIndexed2(poin, value, op))
                    }
                    ReadCycle::IndirectIndexed2(_, lo, op) => {
                        let (lo, carry) = lo.overflowing_add(self.y);
                        CpuState::ReadCycle(ReadCycle::IndirectIndexed3(lo, value, carry, op))
                    }
                    ReadCycle::IndirectIndexed3(lo, hi, carry, op) => {
                        let hi = hi.wrapping_add(carry as u8);
                        let addr = u16::from_le_bytes([lo, hi]);
                        let v = value;
                        match op {
                            OpClass::Read(op) => match carry {
                                true => CpuState::ReadCycle(ReadCycle::Read(addr, op)),
                                false => self.read_op(op, v),
                            },
                            OpClass::ReadModifyWrite(op, read_op) => {
                                CpuState::ReadCycle(ReadCycle::ReadModifyWrite(addr, op, read_op))
                            }
                            OpClass::Write(Store(reg)) => {
                                CpuState::WriteCycle(WriteCycle::Write(addr, self.read_reg(reg)))
                            }
                        }
                    }
                    ReadCycle::ReadModifyWrite(addr, op, read_op) => {
                        CpuState::WriteCycle(WriteCycle::ReadModifyWrite1(addr, value, op, read_op))
                    }
                    ReadCycle::Read(_, op) => self.read_op(op, value),
                    ReadCycle::Branch(cond) => {
                        let offset = value as i8;
                        let b = match cond {
                            Condition::PL => !self.n,
                            Condition::MI => self.n,
                            Condition::VC => !self.v,
                            Condition::VS => self.v,
                            Condition::CC => !self.c,
                            Condition::CS => self.c,
                            Condition::NE => !self.z,
                            Condition::EQ => self.z,
                        };
                        match b {
                            true => CpuState::ReadCycle(ReadCycle::Branch1(offset)),
                            false => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                        }
                    }
                    ReadCycle::Branch1(offset) => {
                        let [lo, hi] = self.pc.to_le_bytes();
                        let (lo, carry) = lo.overflowing_add_signed(offset);
                        self.pc = u16::from_le_bytes([lo, hi]);
                        match carry {
                            true => CpuState::ReadCycle(ReadCycle::Branch2),
                            false => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                        }
                    }
                    ReadCycle::Branch2 => {
                        let [lo, hi] = self.pc.to_le_bytes();
                        // TODO wrapping?
                        let hi = hi.wrapping_add(1);
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::ThrowAway(op) => match op {
                        StackOp::RTI => CpuState::ReadCycle(ReadCycle::RTI1),
                        StackOp::RTS => CpuState::ReadCycle(ReadCycle::RTS1),
                        StackOp::PHA => CpuState::WriteCycle(WriteCycle::PHA),
                        StackOp::PHP => CpuState::WriteCycle(WriteCycle::PHP),
                        StackOp::PLA => CpuState::ReadCycle(ReadCycle::PLA1),
                        StackOp::PLP => CpuState::ReadCycle(ReadCycle::PLP1),
                    },
                    ReadCycle::RTI1 => CpuState::ReadCycle(ReadCycle::RTI2),
                    ReadCycle::RTI2 => {
                        self.write_reg(Register::P, value);
                        CpuState::ReadCycle(ReadCycle::RTI3)
                    }
                    ReadCycle::RTI3 => CpuState::ReadCycle(ReadCycle::RTI4(value)),
                    ReadCycle::RTI4(lo) => {
                        let hi = value;
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::RTS1 => CpuState::ReadCycle(ReadCycle::RTS2),
                    ReadCycle::RTS2 => CpuState::ReadCycle(ReadCycle::RTS3(value)),
                    ReadCycle::RTS3(lo) => {
                        let hi = value;
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::RTS4)
                    }
                    ReadCycle::RTS4 => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                    ReadCycle::PLA1 => CpuState::ReadCycle(ReadCycle::PLA2),
                    ReadCycle::PLA2 => {
                        self.a = value;
                        self.set_n(value);
                        self.set_z(value);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::PLP1 => CpuState::ReadCycle(ReadCycle::PLP2),
                    ReadCycle::PLP2 => {
                        self.write_reg(Register::P, value);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::Interrupt(source) => {
                        CpuState::WriteCycle(WriteCycle::Interrupt1(source))
                    }
                    ReadCycle::Interrupt4(source) => {
                        CpuState::ReadCycle(ReadCycle::Interrupt5(source, value))
                    }
                    ReadCycle::Interrupt5(_, lo) => {
                        let hi = value;
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::JSR => CpuState::ReadCycle(ReadCycle::JSR1(value)),
                    ReadCycle::JSR1(lo) => CpuState::WriteCycle(WriteCycle::JSR2(lo)),
                    ReadCycle::JSR4(lo) => {
                        let hi = value;
                        self.pc = u16::from_le_bytes([lo, hi]);
                        CpuState::ReadCycle(ReadCycle::FetchInstruction)
                    }
                    ReadCycle::Reset => CpuState::ReadCycle(ReadCycle::Reset1),
                    ReadCycle::Reset1 => {
                        CpuState::ReadCycle(ReadCycle::Interrupt(InterruptSource::RESET))
                    }
                }
            }
            CpuState::WriteCycle(op) => match op {
                WriteCycle::ReadModifyWrite1(addr, v, op, read_op) => {
                    let v = self.rmw_op(v, op);
                    if let Some(read_op) = read_op {
                        // This should probably be happening on the next cycle, but should be fine to do here
                        self.read_op(read_op, v);
                    }
                    CpuState::WriteCycle(WriteCycle::Write(addr, v))
                }
                WriteCycle::Write(_, _) => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                WriteCycle::PHA => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                WriteCycle::PHP => CpuState::ReadCycle(ReadCycle::FetchInstruction),
                WriteCycle::Interrupt1(source) => {
                    CpuState::WriteCycle(WriteCycle::Interrupt2(source))
                }
                WriteCycle::Interrupt2(source) => {
                    CpuState::WriteCycle(WriteCycle::Interrupt3(source))
                }
                WriteCycle::Interrupt3(source) => {
                    CpuState::ReadCycle(ReadCycle::Interrupt4(source))
                }
                WriteCycle::JSR2(lo) => CpuState::WriteCycle(WriteCycle::JSR3(lo)),
                WriteCycle::JSR3(lo) => CpuState::ReadCycle(ReadCycle::JSR4(lo)),
            },
        }
    }
}

impl Cpu {
    fn pc_inc(&mut self) -> u16 {
        let t: u16 = self.pc;
        self.pc = self.pc.wrapping_add(1);
        t
    }

    fn stack(&self) -> u16 {
        let addr: u16 = 0x100;
        addr + self.sp as u16
    }

    fn stack_inc(&mut self) -> u16 {
        let mut addr: u16 = 0x100;
        addr += self.sp as u16;
        self.sp = self.sp.wrapping_add(1);
        addr
    }

    fn stack_dec(&mut self) -> u16 {
        let mut addr: u16 = 0x100;
        addr += self.sp as u16;
        self.sp = self.sp.wrapping_sub(1);
        addr
    }

    pub fn read_reg(&self, reg: Register) -> u8 {
        match reg {
            Register::A => self.a,
            Register::X => self.x,
            Register::Y => self.y,
            Register::AX => self.a & self.x,
            Register::S => self.sp,
            Register::P => {
                let mut v: u8 = 1 << 5;
                if self.c {
                    v |= 1 << 0
                }
                if self.z {
                    v |= 1 << 1
                }
                if self.i {
                    v |= 1 << 2
                }
                if self.d {
                    v |= 1 << 3
                }
                if self.v {
                    v |= 1 << 6
                }
                if self.n {
                    v |= 1 << 7
                }
                v
            }
        }
    }

    fn write_reg(&mut self, reg: Register, value: u8) {
        match reg {
            Register::A => self.a = value,
            Register::X => self.x = value,
            Register::Y => self.y = value,
            Register::AX => {
                self.a = value;
                self.x = value;
            }
            Register::S => self.sp = value,
            Register::P => {
                self.c = (value & (1 << 0)) != 0;
                self.z = (value & (1 << 1)) != 0;
                self.i = (value & (1 << 2)) != 0;
                self.d = (value & (1 << 3)) != 0;
                // TODO [1] says PLP loads the B flag, but [2] says the B flag doesn't physically exist
                // [1] https://www.nesdev.org/obelisk-6502-guide/reference.html#PLP
                // [2] https://www.nesdev.org/wiki/Status_flags#The_B_flag
                self.v = (value & (1 << 6)) != 0;
                self.n = (value & (1 << 7)) != 0;
            }
        }
    }

    fn set_z(&mut self, value: u8) {
        self.z = value == 0;
    }

    fn set_n(&mut self, value: u8) {
        self.n = (value & 0x80) != 0;
    }
}

impl Cpu {
    fn read_op(&mut self, op: ReadOp, value: u8) -> CpuState {
        match op {
            ReadOp::Nop => (),
            ReadOp::Load(reg) => {
                self.write_reg(reg, value);
                self.set_z(value);
                self.set_n(value);
            }
            ReadOp::EOR => {
                self.a ^= value;
                self.set_z(self.a);
                self.set_n(self.a);
            }
            ReadOp::AND => {
                self.a &= value;
                self.set_z(self.a);
                self.set_n(self.a);
            }
            ReadOp::ORA => {
                self.a |= value;
                self.set_z(self.a);
                self.set_n(self.a);
            }
            ReadOp::ADC => {
                // use carrying_add once that is in stable
                let (t1, c1) = self.a.overflowing_add(value);
                let (t1, c2) = t1.overflowing_add(self.c as u8);

                let a = self.a as i8;
                let (t2, v1) = a.overflowing_add(value as i8);
                let (t2, v2) = t2.overflowing_add(self.c as i8);
                debug_assert_eq!(t1, t2 as u8);

                self.a = t1;
                self.c = c1 | c2;
                self.v = v1 != v2;
                self.set_z(self.a);
                self.set_n(self.a);
            }
            ReadOp::SBC => {
                // use borrowing_sub once that is in stable
                let (t1, c1) = self.a.overflowing_sub(value);
                let (t1, c2) = t1.overflowing_sub(!self.c as u8);

                let a = self.a as i8;
                let (t2, v1) = a.overflowing_sub(value as i8);
                let (t2, v2) = t2.overflowing_sub(!self.c as i8);
                debug_assert_eq!(t1, t2 as u8);

                self.a = t1;
                self.c = !(c1 | c2);
                self.v = v1 != v2;
                self.set_z(self.a);
                self.set_n(self.a);
            }
            ReadOp::Compare(reg) => {
                let t = self.read_reg(reg);
                // use borrowing_sub once that is in stable
                let (t, c) = t.overflowing_sub(value);
                self.c = !c;

                self.set_z(t);
                self.set_n(t);
            }
            ReadOp::BIT => {
                let t = self.a & value;
                self.set_z(t);
                self.set_n(value);
                self.v = (value & 0x40) != 0;
            }
        };
        CpuState::ReadCycle(ReadCycle::FetchInstruction)
    }

    fn rmw_op(&mut self, v: u8, op: ReadModifyWriteOp) -> u8 {
        match op {
            ReadModifyWriteOp::ASL => {
                let c = (v & 0x80) != 0;
                let v = v.shl(1);
                self.c = c;
                self.set_z(v);
                self.set_n(v);
                v
            }
            ReadModifyWriteOp::ROL => {
                let c = (v & 0x80) != 0;
                let mut v = v.shl(1);
                if self.c {
                    v |= 0x01;
                }
                self.c = c;
                self.set_z(v);
                self.set_n(v);
                v
            }
            ReadModifyWriteOp::LSR => {
                let c = (v & 1) != 0;
                let v = v.shr(1);
                self.c = c;
                self.set_z(v);
                self.set_n(v);
                v
            }
            ReadModifyWriteOp::ROR => {
                let c = (v & 1) != 0;
                let mut v = v.shr(1);
                if self.c {
                    v |= 0x80;
                }
                self.c = c;
                self.set_z(v);
                self.set_n(v);
                v
            }
            ReadModifyWriteOp::DEC => {
                let v = v.wrapping_sub(1);
                self.set_z(v);
                self.set_n(v);
                v
            }
            ReadModifyWriteOp::INC => {
                let v = v.wrapping_add(1);
                self.set_z(v);
                self.set_n(v);
                v
            }
        }
    }
}

impl Cpu {
    pub(crate) fn at_fetch(&self) -> bool {
        matches!(self.state, CpuState::ReadCycle(ReadCycle::FetchInstruction))
    }
}
