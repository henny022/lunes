#[allow(dead_code)]
pub struct INES {
    pub prg_size: u8,
    pub chr_size: u8,
    pub mirroring: Mirroring,
    pub prg_ram: bool,
    pub mapper: u8,
    pub prg_ram_size: u8,
    pub system: System,
}

pub enum Mirroring {
    Horizontal,
    Vertical,
    FourScreen,
}

pub enum System {
    NTSC,
    PAL,
}

impl INES {
    pub fn from_bytes(data: &[u8]) -> Option<Self> {
        if data[0] != 0x4e {
            return None;
        }
        if data[1] != 0x45 {
            return None;
        }
        if data[2] != 0x53 {
            return None;
        }
        if data[3] != 0x1a {
            return None;
        }
        let prg_size = data[4];
        let chr_size = data[5];
        let mut mirroring = if (data[6] & 1) != 0 {
            Mirroring::Vertical
        } else {
            Mirroring::Horizontal
        };
        let prg_ram = (data[6] & 2) != 0;
        if (data[6] & 4) != 0 {
            return None;
        }
        if (data[6] & 8) != 0 {
            mirroring = Mirroring::FourScreen;
        }
        let mut mapper = (data[6] & 0xf0) >> 4;
        mapper |= data[7] & 0xf0;
        let prg_ram_size = data[8];
        let system = if data[9] != 0 {
            System::PAL
        } else {
            System::NTSC
        };
        Some(Self {
            prg_size,
            chr_size,
            mirroring,
            prg_ram,
            mapper,
            prg_ram_size,
            system,
        })
    }
}
