use crate::apu::Apu;
use crate::bus::Bus;
use crate::cart::Cart;
use crate::cpu::Cpu;
use crate::ppu::Ppu;
use crate::ram::Ram;

mod bus;
pub mod cpu;
mod apu;
pub mod ppu;
mod ram;
mod ines;
mod cart;

pub struct NES {
    pub cpu: Cpu,
    pub apu: Apu,
    pub ppu: Ppu,
    pub bus: Bus,
    pub ram: Ram,
    pub cart: Cart,

    master_clock: u32,
}

impl NES {
    pub fn tick(&mut self) {
        if self.master_clock % 12 == 0 {
            self.cpu.phi1(&mut self.bus.cpu);
        } else if self.master_clock % 6 == 0 {
            self.cpu.phi2(&self.bus.cpu);
        }
        if self.master_clock % 4 == 0 {
            self.ppu.tick(&mut self.bus);
        }
        if self.master_clock % 24 == 0 {
            self.apu.tick();
        }
        self.ram.tick(&mut self.bus.cpu);
        self.cart.tick(&mut self.bus);

        self.master_clock = (self.master_clock + 1) % 24;
    }

    pub fn cycle(&mut self) {
        for _ in 0..12 {
            self.tick();
        }
    }

    pub fn step(&mut self) -> u32 {
        let mut cycles = 0;
        loop {
            self.cycle();
            cycles += 1;
            if self.cpu.at_fetch() {
                return cycles;
            }
        }
    }
}

impl NES {
    pub fn with_cart(cart: Cart) -> Self {
        Self {
            cpu: Cpu::default(),
            apu: Apu::default(),
            ppu: Ppu::default(),
            bus: Bus::default(),
            ram: Ram::default(),
            cart,
            master_clock: 0,
        }
    }

    pub fn from_bytes(data: &[u8]) -> Self {
        let cart = cart::from_bytes(data);
        Self::with_cart(cart)
    }

    pub fn from_file(filename: &str) -> Self {
        let cart = cart::from_file(filename);
        Self::with_cart(cart)
    }
}
